<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Pronadji Saigrace | <?php echo $title ?></title>
        <?= link_tag(base_url() . 'css/grid.css'); ?>
        <?= link_tag(base_url() . 'css/grid.responsive.css'); ?>
        <?= link_tag(base_url() . 'css/normalize.css'); ?>
        <?= link_tag('https://fonts.googleapis.com/css?family=Oswald'); ?>
        <?= link_tag('https://fonts.googleapis.com/css?family=Pontano+Sans&amp;subset=latin,latin-ext'); ?>
        <?= link_tag(base_url() . 'css/main.css'); ?>
        <?= link_tag(base_url() . 'css/core.css'); ?>
        <?= link_tag(base_url() . 'css/color.css'); ?>
        <?= link_tag(base_url() . 'css/login.css'); ?>

    </head>
    <body class="theme-orange">
        <div class='container'>
            <?php echo validation_errors('<div class="alert alert-danger">', '</div>'); ?>
            </div>
        <header id="page-header">
            <div class="container">
                <div class="row-fluid">
                    <div class="span12">
                        <a href="<?php echo base_url() ?>" id="logotype">Team7</a>
                        <nav id="primary-nav">
                            <ul>
                                <li><a href="<?php echo  base_url()?>">Početna</a></li>
                                <li>
                                    <a href="<?php echo  base_url()?>search/pretraga/1?sport=Svi&grad=Svi&datumOd=&datumDo=&pol=-&godineOd=&godineDo=">Pretraga</a>
                                </li>
                                <li><a href="<?php echo  base_url()?>Sports_halls/getHalls/1">Sportski objekti</a></li>
                                <li>
                                    <a id="myBtn" href="#">Prijavi se</a>
                                </li>
                                <li><a id="reg" href="#">Registracija</a>
                                </li>
                            </ul>
                        </nav><!-- /primary nav -->
                        <nav id="primary-nav-1">
                            <ul>
                                <li>
                                    <a id="myBtnmob" href="#">Prijavi se</a>
                                </li>
                                <li><a id="regmob" href="#">Registracija</a>
                                </li>
                            </ul>
                        </nav><!-- /primary nav -->
                        <nav id="primary-nav-mobile">
                            <form method="post" action="#" name="mobile-menu">
                                <select id="mobileSelect" onchange="$(this).submit();">
                                    <option value="index.html">Pocetna</option>
                                    <option value="pretraga.html">Pretraga</option>
                                    <option value="kalendar.html">Kalendar</option>
                                </select>
                            </form>
                        </nav>
                    </div>
                </div><!-- /row-fluid -->
            </div>
        </header><!-- /page-header -->

        <section id="content-header">
            <div class="container">
                <div class="row-fluid">
                    <div class="span8">
                        <hgroup class="content-title welcome">
                            <h1>Dorbodošli na sajt za pronalaženje saigrača</h1>
                            <h2>Pravo mesto za organizovaje sportskih događaja</h2>
                        </hgroup>
                    </div>
                    <div class="span4">
                        <hgroup class="fancy-headers">
                            <h1>pratite <span>najnovije</span></h1>
                            <h2>sportske događaje</h2>
                        </hgroup>
                    </div>
                </div>
            </div>
        </section><!-- /content-header -->