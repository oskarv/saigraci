<!DOCTYPE html>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Pronadji Saigrace | <?php echo $title ?></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">

        <?= link_tag(base_url() . 'css/grid.css'); ?>
        <?= link_tag(base_url() . 'css/grid.responsive.css'); ?>
        <?= link_tag(base_url() . 'css/normalize.css'); ?>
        <?= link_tag('https://fonts.googleapis.com/css?family=Oswald'); ?>
        <?= link_tag('https://fonts.googleapis.com/css?family=Pontano+Sans&amp;subset=latin,latin-ext'); ?>
        <?= link_tag(base_url() . 'css/main.css'); ?>
        <?= link_tag(base_url() . 'css/core.css'); ?>
        <?= link_tag(base_url() . 'css/color.css'); ?>
        <?= link_tag(base_url() . 'css/login.css'); ?>
        <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-1.8.2.min.js" ></script>
    </head>
    <body class="theme-orange">
        <?php
        echo form_open('home/login');
        echo form_hidden('page', $this->router->fetch_class());
        echo form_close();
        ?>

        <header id="page-header">
            <div class="container">
                <div class="row-fluid">
                    <div class="span12">
                        <a href="<?php echo base_url() ?>" id="logotype">Team7</a>
                        <nav id="primary-nav">
                            <ul>
                                <li><a href="<?php echo base_url()?>">Početna</a></li>
                                <li><a href="<?php echo  base_url().'term';?>">Dodajte termin</a></li>
                                <li><a href="<?php echo  base_url().'term/deleteTerm';?>">Uklonite termin</a></li>
                                <li>
                                    <a href="<?php echo  base_url()."profile/view/".$this->session->userdata("username");?>" class="withsiblings"><?php echo $username ?> &nbsp;&nbsp;<img width="24" height="24" class="upperRight" src="<?php $data = $this->users_model->get_user($this->session->userdata('username')); echo $data[0]->slika  ?>" alt="" /></a>
                                    <ul>
                                        <li><a href="<?php echo  base_url()."profile/view/".$this->session->userdata("username");?>">Profil</a></li>
                                        <li><?php
                                            echo form_open('home/logout');
                                            echo form_hidden('page',uri_string());
                                            echo "<a href=" . '#' . " onclick=" . "$(this).closest('form').submit()" . ">Odjavi se</a>";


                                            echo form_close();
                                            ?></li>
                                    </ul>
                                </li>
                            </ul>
                        </nav><!-- /primary nav -->
                        <nav id="primary-nav-1">
                            <ul>
                                <li>
                                    <a href="hala_profil.html" class="withsiblings">Korisnik152 &nbsp;&nbsp;<img width="24" height="24" class="upperRight" src="<?php echo base_url() ?>img/common/hala152.jpg" alt="" /></a>
                                </li>
                                <li>
                                    <?php
                                    echo form_open('home/logout');
                                    echo form_hidden('page', $this->router->fetch_class());
                                    echo "<a href=" . '#' . " onclick=" . "$(this).closest('form').submit()" . ">Odjavi se</a>";


                                    echo form_close();
                                    ?>
                                </li>
                            </ul>
                        </nav><!-- /primary nav -->
                        <nav id="primary-nav-mobile">
                            <form method="post" action="#" name="mobile-menu">
                                <select id="mobileSelect" onchange="$(this).submit();">
                                    <option value="index_hala.html">Pocetna</option>
                                    <option value="hala_profil.html">Profil</option>
                                </select>
                            </form>
                        </nav>
                    </div>
                </div><!-- /row-fluid -->
            </div>
        </header><!-- /page-header -->

        <section id="content-header">
            <div class="container">
                <div class="row-fluid">
                    <div class="span8">
                        <hgroup class="content-title welcome">
                            <h1>Dorbodošli na sajt za pronalaženje saigrača</h1>
                            <h2>Pravo mesto za organizovaje sportskih događaja</h2>
                        </hgroup>
                    </div>
                    <div class="span4">
                        <hgroup class="fancy-headers">
                            <h1>pratite <span>najnovije</span></h1>
                            <h2>sportske događaje</h2>
                        </hgroup>
                    </div>
                </div>
            </div>
        </section><!-- /content-header -->