<div class="copyright">
    <div class="post-offset pretraga">
        <!--<a href=<?php echo base_url() ?>><h1 style="text-align: center"> Testiranje! </h1></a>-->
        <form metod = "post" action="<?php echo base_url() ?>search/pretraga/1" >

            Sport:
            <select name="sport">
                <?php
                $a = $q['sports'];
                foreach ($a as $aa) {
                    if ($this->input->get('sport') == $aa)
                        echo "<option selected>" . $aa . "</option>";
                    else
                        echo "<option>" . $aa . "</option>";
                }
                ?>
            </select>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

            Grad
            <select name="grad">
                <?php
                $a = $q['cities'];
                foreach ($a as $aa) {
                    if ($this->input->get('grad') == $aa)
                        echo "<option selected>" . $aa . "</option>";
                    else
                        echo "<option>" . $aa . "</option>";
                }
                ?>
            </select>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

            Od
            <input name="datumOd" type="datetime-local" value="<?php echo $this->input->get('datumOd') ?>"/>

            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

            Do
            <input name="datumDo" type="datetime-local" value="<?php echo $this->input->get('datumDo') ?>"/>

            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

            Za pol: 
            <select name="pol">
                <?php
                $a = array('-', 'M', 'Z');
                foreach ($a as $aa) {
                    if ($this->input->get('pol') == $aa)
                        echo "<option selected>" . $aa . "</option>";
                    else
                        echo "<option>" . $aa . "</option>";
                }
                ?>
            </select>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

            Za godine: 
            <input name="godineOd" type="number" value="<?php echo $this->input->get('godineOd') ?>"/>
            -
            <input name="godineDo" type="number" value="<?php echo $this->input->get('godineDo') ?>"/>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

            <input type="submit" class="buttonAcceptance" value="Traži" >
        </form>
    </div>
</div>
