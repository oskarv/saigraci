<aside id="sidebar" class="span4" style="background-color: #202020;">
    <br/>
    
    <div class="widget live-sets">
        <header>
            <hgroup class="fancy-headers">
                <h2>budite uvek u toku!</h2>
                <h1>posetite naše <span>prijatelje:</span></h1>
            </hgroup>
        </header>
        <div style="text-align:center;margin-bottom: 30px"> 
                <a href="http://www.sportsevents365.com"><img src="http://www.sportsevents365.com/images/logo.png" width="80%" style="margin:15px"></a>
                <a href="http://www.allsportsevents.com"><img src="http://www.allsportsevents.com/Images/LOGO_FINAL%20WHITE_ON_BLACK.gif" width="80%" style="margin:15px"></a>
                <a href="http://www.sportseventsmagazine.com"><img src="http://www.sportseventsmagazine.com/themes/sem2011/images/logo.png" width="80%" style="margin:15px"></a>
                <a href="https://www.mozzartbet.com/#0-0-1"><img src="http://help.mozzartbet.com/img/design/mozzart_logo.png" width="80%" style="margin:15px"></a>

        </div>
    </div><!-- /live-stes -->
    <div class="widget upcoming-events">
        <header>
            <hgroup class="fancy-headers">
                <h1>Dogadjaji u celom <span> svetu </span></h1>
            </hgroup>
        </header>

        <div class="flyer-wrapper" style="margin:20px">
            <p class="event-date">April 10, 2016</p>
            <p class="view-event">Hapalua – Hawaii’s Half Marathon</p>
            <figure>
                <a href="http://www.fullofevents.com/city/oahu/event/2016-hapalua-hawaiis-half-marathon/" title=""><img src="http://www.fullofevents.com/wp-content/uploads/2016/03/12113356_970400883023586_3807593145536853094_o.jpg"/></a>
            </figure>
        </div>
        <div class="flyer-wrapper" style="margin:20px; margin-top:0px">
            <p class="event-date">April 2, 2016</p>
            <p class="view-event">Annual Ford Island Bridge Run</p>
            <figure>
                <a href="http://www.fullofevents.com/city/oahu/event/19th-annual-ford-island-bridge-run/" title=""><img src="http://www.fullofevents.com/wp-content/uploads/2016/03/12615285_1284742168218382_4111874661121357670_o.jpg"/></a>
            </figure>
        </div>
        <div class="flyer-wrapper" style="margin:20px; margin-top:0px">
            <p class="event-date">14 August 2016</p>
            <p class="view-event">2016 Top End Gran Fondo</p>
            <figure>
                <a href="http://travelnt.com/en/darwin-and-surrounds/events/2016-top-end-gran-fondo" title=""><img src="http://travelnt.com/~/media/atdw/darwin-and-surrounds/events/9002269/images/tnt_landscape__9056901_nttc_eem3.ashx?bc=white"/></a>
            </figure>
        </div>
        
    </div><!-- /upcoming-events -->

    <span class="flyer-wrapper" style="margin:0px;">
            <figure>
                <img style="position:relative; top:5px" src="<?php echo base_url()."img/common/side_end.png"?>" alt=""/>
            </figure>
        </span>
   
    
</aside>


