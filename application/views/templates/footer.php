
<footer id="page-footer">

    <div class="container">
        <div class="row-fluid">
            <div class="span4">
                <div>
                    <div class="fancy-headers"><h3>Dodatne informacije? <span>Kontaktirajte nas</span></h3></div>
                    <div class="vcard">
                        <span class="fn">Pronađi saigrače &amp; DOO</span><br/>
                        <div class="adr">
                            <span class="street-address">Bulevar Kralja Aleksandra 73 - ETF</span>
                            <span class="extended-address">Vukov Spomenik</span><br/>
                            <span class="locality">Beograd</span>,
                            <abbr class="region">Opština Palilula</abbr>
                            <span class="postal-code">11000</span>
                        </div>
                        <div class="tel">063 000 000</div>
                        <div><a class="email" href="mailto:pomoc@pronadjisaigrace.com">pomoc@pronadjisaigrace.com</a></div>
                    </div>
                </div>
                <div>
                    <div class="fancy-headers"><h3>Imate <span>predlog?</span></h3></div>
                    <p>Za primedbe, sugestije, pohvale i predloge kontaktirajte broj:</p>
                    <div class="vcard">
                        <div class="tel">telefon: 011 120 320</div>
                    </div>
                </div>
            </div>

            <div class="span4">
                <div>
                    <div class="fancy-headers"><h3>Naši zaposleni:</h3></div>
                    <p>Ljudi koji osiguravaju najpoželjnije iskustvo na našem sajtu:</p>
                    <ul class="staff-members">
                        <li>
                            <p class="bubble">Stefan Kostić</p>
                            <figure><img src="<?php echo base_url()?>img/common/staff01.jpg" alt="staff01" width="" height="" /></figure>
                        </li>
                        <li>
                            <p class="bubble">Oskar Vatić</p>
                            <figure><img src="<?php echo base_url()?>img/common/staff02.jpg" alt="staff02" width="" height="" /></figure>
                        </li>
                        <li>
                            <p class="bubble">Nemanja Mikić</p>
                            <figure><img src="<?php echo base_url()?>img/common/staff03.jpg" alt="staff03" width="" height="" /></figure>
                        </li>
                        <li>
                            <p class="bubble">Vidoje Zeljić</p>
                            <figure><img src="<?php echo base_url()?>img/common/staff04.jpg" alt="staff04" width="" height="" /></figure>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="span4">
                <div>
                    <div class="fancy-headers"><h3>Društvene mreže</h3></div>
                    <p>Podeli ovo neverovatno iskustvo sa prijateljima na društvenim mrežama!</p>
                    <ul class="social-hub">
                        <li><a href="#" class="share"><span>Share</span></a></li>
                        <li><a href="http://www.facebook.com" class="facebook-share"><span>Share on Facebook</span></a></li>
                        <li><a href="http://www.twitter.com" class="twitter-share"><span>Share on Twitter</span></a></li>
                    </ul>
                </div>
                <div>
                    <div class="fancy-headers"><h3>Najnoviji Twitovi</h3></div>
                    <p class="latest-tweet">Ne propusti ni jedan sportski događaj! Lajkuj nas na Facebook-u i prati na Twitter-u! <a href="http://www.facebook.com">#facebook</a> <a href="http://www.twitter.com">#twitter</a></p>
                </div>
            </div>

        </div>
    </div>

    <div class="copyright">2016 Sva prava zadržana <a href="http://si3psi.etf.rs">ETF - Principi softverskog inženjerstva</a>. <span class="author">Napravio <a href="#" title="">Team7</a></span></div>
</footer>

</body>
</html>