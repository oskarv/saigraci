<?php
$this->load->view($header);
if ($page == "Search_view")
    $this->load->view($search);
echo '<section id="content-container" class="container"><div class="row-fluid">';
$this->load->view($page);
if ($reklame) $this->load->view($side);
echo '</div></section><!-- content-container -->';
if(!$is_logged_in)
$this->load->view("templates/login_form");
$this->load->view("templates/footer");
?>
