<section id="content" class="span8 blog posts">

<?php 
	echo '

       <article class="post single">
    	<div class="post-offset">
		<figure class="post-icon"><a href="" rel="lightbox"><img src="'.$rezultat->slika.'" alt="" width="1000px"/></a></figure>
    	<h1></h1>
    	<h1>'.$rezultat->naslov.'</h1>
		<h2>Sport: '.$rezultat->sport.'</h2>
		<br/>
		<h2>Opis:</h2>
    	<p>'.$rezultat->opis.'</p>
		<br/>
		<h2>Godine&nbsp&nbsp&nbsp od: &nbsp&nbsp'.$rezultat->godine_od.'&nbsp&nbsp do: &nbsp&nbsp'.$rezultat->godine_do.'</h2>
		
		<br/>
		<h2>Trenutni ucesnici</h2>		
		
    	<table>
    		<thead>
    			<tr>
    				<th>Broj</th>
    				<th>Ime i prezime</th>
    				<th>Datum rodjenja</th>
    				<th>Pol</th>
					<th>Status</th>
    			</tr>
    		</thead>
			
			'
			?>
			
    		<tbody>
			
			<?php 
			
			$i = 1;
			
			foreach($rezultat1 as $row) {
			echo '<tr> <td>'.$i++.'</td>';
			echo '<td>'.$row->ime.'</td>';
			echo '<td>'.$row->datum.'</td>';
			echo '<td>'.$row->pol.'</td>';
			echo '<td>'.$row->status.'</td></tr>';
			
			}
			?>
    			
    		</tbody>
    	</table>
		<br/>
		
		
		<?php 	
			
			if ($this->arr['is_logged_in']==true){
			if($rezultat->idKO == $rezultat4->idK)
				echo '
				<br/>
				<form method="link" action="/event/delete_ev/'.$rezultat->idD.'">
					<input type="submit" class="buttonAcceptance"value="Otkazi dogadaj" >
				</form>
				<br/><br/>						
					';
					}
                                ?>
		
		<?php
				
			if ($this->arr['is_logged_in']==true){
				if($rezultat->idKO != $rezultat4->idK){
					if (count($rezultat2) > 0) {
						} else { 
							echo '
							
								<a href="'.base_url().'/event/register_for_ev/?dog='.$rezultat->idD.'">
									<input type="submit" class="buttonAcceptance"value="Prijavi se" >
								</a>
								';
						}
					}
				}
								?>
		
		
		<br/>
		<!--  <input type="submit" class="buttonAcceptance"value="Zaustavi prijave" >  -->
   						
		
		
    	<div class="post-options">
    		<ul class="social">
    			<li><a href="#" class="share">Share</a></li>
    			<li><a href="#" class="facebook-share">Share on Facebook</a></li>
    			<li><a href="#" class="twitter-share">Share on Twitter</a></li>
    		</ul>
    	</div>
		
    	
    	<?php if($br_kom > 0) echo'
    	<h3 id="nema">Komentari</h3>
    	';
		else echo '<h3 id="nema">Nema komentara za dati dogadjaj</h3>';
		?>
    	<ul id="comments" >
			
			<?php
			foreach($komentari as $row) { echo'
			<li class="comment">
				<p class="comment-author" style="height : 50px">
							<img width="48" height="48" class="avatar avatar-48 photo" src="'.$row->slika.'" alt="" />
				<cite><a class="url" rel="external nofollow" href="'.base_url().'profile/view/'.$row->username.'">'.$row->username.'</a></cite>
				<br><small class="comment-time">'.$row->datum.'</small>
						</p>';
						if($is_logged_in)
						if($rezultat4->idK==$row->idK) echo'
						<input type="button" onclick="deleteComment('.$row->idKom.')" style="float:right" class="btn" value="Obrisi" name="submit">
						';
						echo'
						<div class="commententry">
							<p>'.nl2br($row->tekst).'</p>
				</div>
			</li>
			';			
			}?>
			
        </ul>
		
			<form type='link' style='width:100%; text-align: center; display:block'>
				<p><input id='vise' <?php if($br_kom <= 5) echo"type='hidden'"; else echo "type='button'"; ?> class='vise btn btn-medium' value='Ucitaj jos komentara'/></p>
			</form>
			
		<?php if($is_logged_in && $header=="templates/headers/header_user"){ ?>
    	
    	<h3>Ostavite komentar</h3>
    	
    	<div id="respond">
	    	<form id="commentform" class="commentform">
	    		<p class="comment-form-comment"><textarea class="required" style="width:100%" aria-required="true" rows="8" name="comment" id="comment"></textarea></p>
	    		<p class="form-submit"><input type="button" value="Postavi" id="submit" class="btn btn-medium" name="submit"></p>
		    </form>
		</div>

		<?php } ?>

    	
    	</div>
    </article><!-- /post -->
    
</section><!-- /content -->

<script>
	
	window.num = 1;
	window.evnt = <?php echo $eventNum?>;
	broj = parseInt(<?php echo $br_kom?>);
		
	function moreComments() {
		$('input.vise').on('click', function () {
			window.num++;
			$.post('<?php echo base_url()?>event/comments/' + window.evnt.toString() + '/' + window.num.toString(), function (data) {
				var obj = JSON.parse(data);
				var res = $(obj.output).html();
				$('#comments').html(obj.output);
				if (obj.num_of_comments <= window.num*5) {
					document.getElementById("vise").type="hidden";					
				}
			});
		});
	};
	
	function addNewComment(){
		$('#submit').on('click', function (submit) {		
			submit.preventDefault();
			$.post('<?php echo base_url()."event/addComment/".$eventNum; ?>', $('form.commentform').serialize(), function(data2){
				broj++;
				window.num--;
				document.getElementById("vise").type="button";
				document.getElementById("vise").click();
				document.getElementById("comment").value = '';
				document.getElementById("nema").innerHTML = "";
				document.getElementById("nema").innerHTML = "Komentari";
			});		
		});
	};
	
	function deleteComment(id){	
		broj--;
		$.post('<?php echo base_url()?>event/delComment/'+id.toString(), function(){
				window.num--;
				document.getElementById("vise").click();
				if(broj == 0) 
					document.getElementById("nema").innerHTML = "Nema komentara za dati dogadjaj";
			});				
	};
	
	
	<?php if($is_logged_in){ ?>
    jQuery(document).ready(function () {
    	moreComments();
		addNewComment();
	});
    <?php }?>
	
</script> 
