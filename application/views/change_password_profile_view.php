        <section id="content" class="span8 blog posts">

            <article class="post single">
            
                <form action="<?php echo base_url()."profile/update_password"?>" method="post" >
                    <div class="Errormsg"></div>
                    <table>
                        <tr>
                            <td class="ndata">Stara šifra: </td>
                            <td class="data">
                                <input name="oldpass" type="password" class="textreg" value="">
                            </td>
                        <tr>
                        <tr>
                            <td class="ndata">Nova šifra: </td>
                            <td class="data">
                                <input name="newpass" type="password" class="textreg" value="">
                            </td>
                        <tr>
                        <tr>
                            <td class="ndata">Potvrdi šifru: </td>
                            <td class="data">
                                <input name="ppass" type="password" class="textreg" value="">
                            </td>
                        <tr>
                    </table>
                    <input type="submit" class="buttonAcceptance" style="position:relative; left:15px;" value="Promeni" >
                </form>
            </article><!-- /post -->

        </section><!-- /content -->

<script>
    jQuery(document).ready(function () {
        $('form').on('submit', function (form) {
            form.preventDefault();
            $.post('update_password', $('form').serialize(), function (data) {
                var res = $(data).html();
                if (res == "success") {
                    window.location="view/<?php echo $this->session->userdata('username');?>";
                } else {
                    $('div.Errormsg').html(data);
                }
            });
        });
    });
</script>