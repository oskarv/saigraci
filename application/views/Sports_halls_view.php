


<section id="content" class="span8 front-page posts">

    <?php
    $num = 0;
    if ($q['result'])
        foreach ($q['result'] as $row) :
            ?>
            <article class="post" style="margin: 0px">
                <div class="post-offset">

                    <div class="flyer-wrapper" style="margin: 0px">
                        <p class="view-event"><span><a style="color:white; font-size: 18px; font-family: 'Segoe UI'; font-style: italic"><?php echo htmlspecialchars($row->ime) ?></a></span></p>
                        <a id="borderEvent" href="<?php echo base_url() . "profile/view/" . $row->username ?>"><figure style="border: 10px solid;background-image: url('<?php echo $row->slika; ?>');height:200px; width:95%;background-position: center; background-size: cover;border-radius:10px" class="post-icon"></figure></a>
                    </div>

                </div>
            </article><!-- /post -->
        <?php
        endforeach;
    else {
        ?> <h2>Za zadate parametre ne postoji nijedan dogadjaj!</h2> <?php }; ?>

    <section id="pagination">
        <ol>             
            <li><a href="<?php if ($current != 1) echo base_url() . "Sports_halls/getHalls/" . ($current - 1) ?>" title="">&laquo;</a></li>

            <?php
            $num = ceil($q['numOfRows'] / 6);
            for ($i = 1; $i <= $num; $i++) {
                ?>                            
                <li><a href="<?php echo base_url() . "Sports_halls/getHalls/" . $i ?>" title=""  <?php if ($current == $i) echo "class='current'" ?>> <?php echo $i; ?> </a></li>
<?php } ?>

            <li><a href="<?php if ($current != $num) echo base_url() . "Sports_halls/getHalls/" . ($current + 1) ?>" title="">&raquo;</a></li>
        </ol>
    </section>
</section><!-- /content -->
