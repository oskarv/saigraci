        <section id="content" class="span8 blog posts">

            <article class="post single">

                <form action="<?php echo base_url() . "profile/update" ?>">
                    <div class="Errormsg"></div>
                    <table>
                        <tr>
                            <td class="ndata">Promeni ime i prezime: </td>
                            <td class="data">
                                <input id="mreg" name="mreg" type="text" class="textreg" value="<?php echo $ime; ?>">
                            </td>
                        </tr>
                        <tr>
                            <td class="ndata">Promeni sliku: </td>
                            <td class="data">
                                <input id="sreg" name="sreg" type="text" class="textreg" value="<?php echo $slika; ?>">
                            </td>
                        </tr>
                        <tr>
                            <td class="ndata">Promeni e-mail: </td>
                            <td class="data">
                                <input id="ereg" name="ereg" type="text" class="textreg" value="<?php echo $email ?>">
                            </td>
                        </tr>
                        <tr>
                            <td class="ndata">Promeni datum rođenja: </td>
                            <td>
                                <input id="dreg" name="dreg" type="date" class="textreg" value="<?php echo $godiste ?>">
                            </td>
                        </tr>
                        <tr>
                            <td class="ndata">Promeni pol: </td>
                            <td class="none">
                                <input type="radio" name="gender" value="male" <?php if ($pol == 'm') echo "checked" ?>> Muski
                                <input type="radio" name="gender" value="female" <?php if ($pol == 'f') echo "checked" ?>> Zenski<br/>
                            </td>
                        </tr>
                        <tr>
                            <td class="ndata">Promeni broj telefona: </td>
                            <td class="data">
                                <input id="breg" name="breg" type="text" class="textreg" value="<?php echo $tel ?>">
                            </td>
                        </tr>
                        <tr>
                            <td class="ndata">Promeni opis: </td>
                            <td>
                                <div id="respond">
                                    <textarea class="required" aria-required="true" rows="8" cols="47" name="opis" id="comment"><?php echo $opis ?></textarea>
                                </div>					
                            </td>
                        </tr>
                    </table>
                    <input type="submit" class="buttonAcceptance" style="position:relative; left:15px;" value="Promeni" > <br/><br/>&nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="<?php echo base_url() . "profile/change_password" ?>"> Promeni SIfru </a>
                </form>
            </article><!-- /post -->

        </section><!-- /content -->

<script>
    jQuery(document).ready(function () {
        $('form').on('submit', function (form) {
            form.preventDefault();
            $.post('update', $('form').serialize(), function (data) {
                var res = $(data).html();
                if (res == "success") {
                    window.location="view/<?php echo $this->session->userdata('username');?>";
                } else {
                    $('div.Errormsg').html(data);
                }
            });
        });
    });
</script> 
