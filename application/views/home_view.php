<section id="content" class="span8 front-page posts">

    <?php
    $num = 0;
    if ($q['result'])
        foreach ($q['result'] as $row) :
            ?>
            <article class="post" style="margin: 0px">
                <div class="post-offset">

                    <div class="flyer-wrapper" style="margin: 0px">
                        <p class="event-date"><span style="font-style:italic; font-family: 'Segoe UI'"><?php echo $row->datumOd ?></span></p>
                        <p class="view-event" style="bottom:43px;"><a style="color:white" href="<?php echo base_url() . "event/dogadjaj/" . $row->idD ?>"><?php echo strtoupper(htmlspecialchars($row->naslov)) ?></a></p>
                        <a id="borderEvent" href="<?php echo base_url() . "event/dogadjaj/" . $row->idD ?>"><figure style="border: 10px solid;background-image: url('<?php echo $row->dogSlika; ?>');height:200px; width:95%;background-position: center; background-size: cover;border-radius:10px" class="post-icon"></figure></a>
                    </div>

                    <h6 style="float:right; font-size:10px; position:relative; bottom:48px; right:10px; background-color: black; padding:5px;border-radius: 5px;font-style: italic"><span style="font-size: 10px">Autor:</span> <a class="fancy-headers" href="<?php echo base_url() . "profile/view/" . htmlspecialchars($row->username) ?>"><?php echo $row->ime ?></a>&nbsp;&nbsp;&nbsp;;
                        <span style="font-size: 10px">Hala:</span> <a class="fancy-headers" href="<?php echo base_url() . "profile/view/" . htmlspecialchars($row->husername) ?>"><?php echo $row->hime ?></a></h6>
                </div>
            </article><!-- /post -->
        <?php
        endforeach;
    else {
        ?> <h2>Za zadate parametre ne postoji nijedan dogadjaj!</h2> <?php }; ?>


</section><!-- /content -->
