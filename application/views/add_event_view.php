		<section id="content" class="span8 blog posts">

				    <article class="post single">
	
			<table>
				
				<tr>
				<form type="link" class ="dogtermini">
					<td class="ndata">Datum: </td>
					<td>
						<input name="dreg" type="datetime-local" class="textreg" value="Datum"> &nbsp&nbsp&nbsp
						<input type="submit" class="buttonAcceptance" style="position:relative; left:15px;" value="Potvrdi" >
					</td>
				</form>
				</tr>
				
				<form method="POST" id="forma" action="<?php echo base_url() ?>event/create">
			
				<tr>
					<td class="ndata">Naziv događaja: </td>
					<td class="data">
						<input name="nazivdog" type="text" class="textreg" value="Naziv">
					</td>
				</tr>
				<tr>
					<td class="ndata">Link do slike: </td>
					<td class="data">
						<input name="slika" type="text" class="textreg" value="url">				
					</td>
				</tr>
				<tr>
					<td class="ndata">Sport: </td>
					<td class="data">
						<input name="sport" type="text" class="textreg" value="Sport">
					</td>
				</tr>
				<!--
				<tr>
				<form type="link" class ="dogtermini">
					<td class="ndata">Datum: </td>
					<td>
						<input name="dreg" type="datetime-local" class="textreg" value="Datum"> &nbsp&nbsp&nbsp
						<input type="submit" class="buttonAcceptance" style="position:relative; left:15px;" value="Potvrdi" >
					</td>
				</form>
				</tr>
				!-->
				<tr>
					<td class="ndata">Rezerviši: </td>
					<td>
						<div class="post-offset">
								<div type="link" action="">
								<table>
									<thead>
										<tr>
											<th>Broj</th>
											<th>Hala</th>
											<th>Termin</th>
											<th>Cena po satu</th>
											<th style="text-align: center;">Rezervacija</th>
										</tr>
									</thead>
									<tbody  class="terminihala">
										<tr>
											<td>-</td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
										</tr>
									</tbody>
								</table>
								</div>
							</div>
					</td>
				</tr>
				<tr>
					<td class="ndata">Godine saigrača: </td>
					<td>
						<table style="width:100%; padding:0px; border-spacing: 4px;">
							<tr>
								<td class="data">
									<input name="godine_od" type="text" class="textreg" value="Od">
								</td>
								<td class="data">
									<input name="godine_do" type="text" class="textreg" value="Do">
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="ndata">Pol saigrača: </td>
					<td class="none">
						<input type="radio" name="gender" value="male"> Muski
						<input type="radio" name="gender" value="female"> Zenski
						<input type="radio" name="gender" value="nebitno" checked> Nebitno <br/>
					</td>
				</tr>
				<tr>
					<td class="ndata">Opis / dodatne napomene: </td>
					<td>
						<div id="respond">
							<p class="comment-form-comment"><textarea class="required" name="komentar" aria-required="true" rows="8" cols="47" ></textarea></p>						    
						</div>					
					</td>
				</tr>
			</table>
				<input type="submit" class="buttonAcceptance" style="position:relative; left:15px;" value="Napravi" >
			</form>
				    </article><!-- /post -->
				    
		</section><!-- /content -->

<script>
	$('form.dogtermini').on('submit',function(form){
	form.preventDefault();
	$.post('<?php echo base_url()?>event/dogadjajtermini',$('form.dogtermini').serialize(),function(data){
		var res = $(data).html();
		$('tbody.terminihala').html(data);

	});
});
</script>