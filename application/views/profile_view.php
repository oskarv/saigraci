        <section id="content" class="span8 blog posts">

            <article class="post single">

                <table>
                    <tr>
                        <td>
                            <figure class="post-icon"><a href="<?php echo $slika?>" rel="lightbox"><img style="border-radius:25px; border:8px solid #1f1f1f;"src="<?php echo $slika ?>" alt="image not loaded" width="200px"/></a></figure>
                        </td>
                        <td>
                            <table class="tabela">
                                <tr>
                                    <td>
                                        <div class="post-offset fancy-headers">
                                            <h5>Ime i prezime:</h5>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="post-offset">
                                            <p><?php echo $ime ?></p>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="post-offset fancy-headers">
                                            <h5>Pol:</h5>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="post-offset">
                                            <p><?php
                                                if ($pol == 'm')
                                                    echo "Muski";
                                                else {
                                                    echo "Zenski";
                                                }
                                                ?></p>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="post-offset fancy-headers">
                                            <h5>Godiste: </h5>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="post-offset">
                                            <p><?php echo $godiste ?></p>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="post-offset fancy-headers">
                                            <h5>email: </h5>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="post-offset">
                                            <p><?php echo $email ?></p>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="post-offset fancy-headers">
                                            <h5>Telefon: </h5>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="post-offset">
                                            <p><?php echo $tel; ?></p>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <div class="post-offset">
                    <h1>O korisniku:</h1>
                    <p><?php echo $opis; ?></p>
                    <h2 class="fancy-headers"> Prosečna ocena:&nbsp;&nbsp;&nbsp; <?php echo $ocena; ?> 
                        <span style='color:lightgray; font-size:10px;position:relative; left:15px;'>
                            <?php
                            echo $brojGlasova;
                            if ($brojGlasova == 1) {
                                echo " glas";
                            } else if ($brojGlasova < 5 && $brojGlasova > 0) {
                                echo " glasa";
                            } else
                                echo " glasova"
                                ?>
                        </span>

                    </h2> 

                </div>
                <?php
                if ($this->session->userdata('username') == $username) {
                    echo form_open(base_url() . "profile/change");
                    $atributes = array('class' => 'buttonAcceptance', 'value' => 'Promeni lične podatke', 'style' => 'position:relative; left:15px;');
                    echo form_submit($atributes);
                    echo form_close();
                } else {
                    if ($this->session->userdata('username') != null && $this->users_model->get_role()) { // ako je ulogovan i plus je osoba
                        echo form_open(base_url() . "profile/rate/" . $username);
                        echo "<span style='color:white;position:relative; left:15px;'>Ocenite korisnika: </span>";
                        $options = array(
                            '0' => '0',
                            '1' => '1',
                            '2' => '2',
                            '3' => '3',
                            '4' => '4',
                            '5' => '5'
                        );
                        echo form_dropdown('ocena', $options, '0', 'style="position:relative; left:15px;"');
                        echo "<br/><br/>";
                        $atributes = array('class' => 'buttonAcceptance', 'value' => 'Oceni', 'style' => 'position:relative; left:15px;');
                        echo form_submit($atributes);
                        echo form_close();
                    }
                }
                ?>
            </article><!-- /post -->

        </section><!-- /content -->