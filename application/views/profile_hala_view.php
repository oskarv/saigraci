        <section id="content" class="span8 blog posts">

            <article class="post single">

                <table>
                    <tr>
                        <td>
                            <figure class="post-icon"><a href="<?php echo $slika?>" rel="lightbox"><img style="border-radius:25px; border:8px solid #1f1f1f;"src="<?php echo $slika ?>" alt="image not loaded" width="250px"/></a></figure>
                        </td>
                        <td>
                            <table class="tabela">
                                <tr>
                                    <td>
                                        <div class="post-offset fancy-headers">
                                            <h5>Naziv:</h5>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="post-offset">
                                            <p><?php echo $ime?></p>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="post-offset fancy-headers">
                                            <h5>email: </h5>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="post-offset">
                                            <p><?php echo $email?></p>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="post-offset fancy-headers">
                                            <h5>Adresa: </h5>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="post-offset">
                                            <p><?php echo $adresa?></p>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="post-offset fancy-headers">
                                            <h5>Telefon: </h5>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="post-offset">
                                            <p><?php echo $tel?></p>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <div class="post-offset">
                    <h1>O hali:</h1>
                    <p><?php echo $opis?></p>
                    <h1>Galerija:</h1>
                    <div id="galerija">
                        <?php 
                            foreach($gallery as $row) {
                                if ($row != null) {
                                echo "<a href='".$row->slika."'><img style='padding:10px' src='".$row->slika."' width='30%'></a>";
                                }
                            }
                        ?>                        
                    </div>
                </div>
            </article><!-- /post -->
                        <?php
                        if ($this->session->userdata('username') == $username) {
                            echo form_open(base_url() . "profile/change");
                            $atributes = array('class' => 'buttonAcceptance', 'value' => 'Promeni podatke', 'style' => 'position:relative; left:15px;');
                            echo form_submit($atributes);
                            echo form_close();
                        }
                        ?>
            <br/><br/>
        </section><!-- /content -->
