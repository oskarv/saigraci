<section id="content" class="span8 blog posts">

            <article class="post single">

                <form action="<?php echo base_url() . "profile/update" ?>">
                    <div class="Errormsg"></div>
                    <table>
                        <tr>
                            <td class="ndata">Promeni naziv: </td>
                            <td class="data">
                                <input id="mreg" name="mreg" type="text" class="textreg" value="<?php echo $ime; ?>">
                            </td>
                        </tr>
                        <tr>
                            <td class="ndata">Promeni sliku: </td>
                            <td class="data">
                                <input id="sreg" name="sreg" type="text" class="textreg" value="<?php echo $slika; ?>">
                            </td>
                        </tr>
                        <tr>
                            <td class="ndata">Promeni e-mail: </td>
                            <td class="data">
                                <input id="ereg" name="ereg" type="text" class="textreg" value="<?php echo $email ?>">
                            </td>
                        </tr>
                        <tr>
                            <td class="ndata">Promeni adresu: </td>
                            <td class="data">
                                <input id="areg" name="areg" type="text" class="textreg" value="<?php echo $adresa ?>">
                            </td>
                        </tr>    
                        <tr>
                            <td class="ndata">Promeni broj telefona: </td>
                            <td class="data">
                                <input id="breg" name="breg" type="text" class="textreg" value="<?php echo $tel ?>">
                            </td>
                        </tr>
                        <tr>
                            <td class="ndata">Promeni opis: </td>
                            <td>
                                <div id="respond">
                                    <textarea class="required" aria-required="true" rows="8" cols="47" name="opis" id="comment"><?php echo $opis ?></textarea>
                                </div>					
                            </td>
                        </tr>
                    </table>
                    <h3 style="color:white;position:relative; left:15px;">Galerija: <input type="button" class="buttonAcceptance" style="position:relative; left:15px;" value="Dodaj polje" onclick="addMore()" /></h3>
                    <table id='galerija' style="width:100%">
                        <?php 
                            
                            foreach($gallery as $row) {
                                if ($row != null) {
                                echo "<tr>"
                                        . "<td class='data' style='width:100%'>"
                                            . "<input name='' type='text' class='textreg' value='".$row->slika."' disabled>"
                                        . "</td>"
                                        ."<td>"
                                            ."<a id='del' href='".base_url()."profile/delete_gallery_image/".$row->idG."'>Obrisi</a>"
                                        ."</td>"
                                   . "</tr>";
                                }
                            }
                        ?>
                    </table>
                    <br/>
                    <p style="color:lightgray; font-size: 10px; position:relative; left:15px;">Napomena: los link koji ne vodi do slike nece biti sacuvan!</p>
                    <hr/>
                    <input type="submit" class="buttonAcceptance" style="position:relative; left:15px;" value="Promeni" > <br/><br/>&nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="<?php echo base_url() . "profile/change_password" ?>"> Promeni SIfru </a>
                </form>
            </article><!-- /post -->

        </section><!-- /content -->

<script>
    jQuery(document).ready(function () {
        $('form').on('submit', function (form) {
            form.preventDefault();
            $.post('update', $('form').serialize(), function (data) {
                var res = $(data).html();          
                if (res == "success") {
                    window.location = "view/<?php echo $this->session->userdata('username'); ?>";
                } else {
                    $('div.Errormsg').html(data);
                }
            });
        });
    });

</script>

<script>
    id = 1;
    function addMore() {
        galerija = document.getElementById('galerija');
        var tr = document.createElement("tr");
        var td = document.createElement("td");
        td.className = 'data';
        td.style = 'width:100%';
        var input = document.createElement("input");
        input.name = 'gal[]';
        input.value = 'Ovde ubaciti link slike';
        input.type = 'text';
        input.className = "textreg";
        galerija.appendChild(tr);
        tr.appendChild(td);
        td.appendChild(input);
    }
</script>