<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once(APPPATH . 'controllers/MyController.php');
class Term extends MyController {
    
     function __construct() {
        parent::__construct();
        parent::index();
    }

    public function index() {
        if ($this->arr['is_logged_in']==true){
            $this->arr['page']="Term_create_view";
            $this->arr['check'] = "";
            $this->load->view("templates/page", $this->arr);
        }else{
            redirect(base_url());
        }
    } 

    public function create (){
        if ($this->arr['is_logged_in']==true){
            $this->arr['page']="Term_create_view";
            $this->load->model('Term_model');
            $this->arr['check'] = $this->Term_model->create();            
            $this->load->view("templates/page", $this->arr);
        }else{
            redirect(base_url());
        }
    }
    
    public function deleteTerm(){
         if ($this->arr['is_logged_in']==true){
            $this->arr['page']="Term_delete_view";
            $this->load->model('Term_model');
            $this->arr['currentTerms'] = $this->Term_model->getAllTerms();
            $this->load->view("templates/page", $this->arr);
        }else{
            redirect(base_url());
        }
    }
    
    public function delTerm($id){
		$this->load->model('Term_model');
		$this->Term_model->delTerm($id);
        
        $q = $this->Term_model->getAllTerms();
        echo'
         <table style="width:100%">
            <tr style="color:white">
                <td> <h3> Datum od </h3> </td>
                <td> <h3> Datum do </h3> </td>
                <td> <h3> Cena </h3> </td>
                <td>  </td>
            </tr>
        ';
        foreach($q as $row) { echo'
         <tr style="color:white">
            <td> '. $row->datumOd. ' </td>
            <td> '. $row->datumDo. ' </td>
            <td> '. $row->cena .' </td>
            <td> <input type="button" id="obrisi" onclick="deleteTerm('.$row->idT.')" style="float:right" class="btn" value="Obrisi" name="submit"> </td>
        </tr>
        ';
        }
        echo'
        </table>
        ';
	}
    
}