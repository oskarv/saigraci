<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once(APPPATH . 'controllers/MyController.php');

class Profile extends MyController {

    function __construct() {
        parent::__construct();
        parent::index();
    }
    
    public function index() {
        redirect(base_url());
    }

    public function view($username) {                                           // pregled profila
        if (!$username) {
            redirect(base_url());
            return;
        }
        $data = $this->users_model->get_user($username);
        if (!$data) {
            redirect(base_url());
            return;   
        }
        $this->arr['username'] = $username;
        $this->arr['ime'] = $data[0]->ime;
        $this->arr['email'] = $data[0]->email;
        $this->arr['tel'] = $data[0]->telefon;
        $this->arr['opis'] = $data[0]->opis;
        $this->arr['slika'] = $data[0]->slika;
        if ($this->users_model->get_role_user($data[0]->idK)) {                 // korisinik je osoba
            $this->arr['page'] = "profile_view";
            $this->arr['pol'] = $data[0]->pol;
            $this->arr['godiste'] = $data[0]->datum;
            $this->arr['ocena'] = $this->users_model->get_ocena($data[0]->idK);
            $this->arr['brojGlasova'] = $this->users_model->get_br_glasova($data[0]->idK);
        } else {                                                                // korisnik je hala           
            $this->arr['page'] = "profile_hala_view";
            $this->arr['adresa'] = $data[0]->adresa;
            $this->arr['gallery'] = $this->gallery_model->get_gallery($data[0]->idK);    
        }
        $this->load->view("templates/page", $this->arr);
    }

    public function change() { // promena profila
        $username = $this->session->userdata('username');
        if (!$username) {
            redirect(base_url());
            return;
        }
        $data = $this->users_model->get_user($username);
        $this->arr['ime'] = $data[0]->ime;
        $this->arr['email'] = $data[0]->email;
        $this->arr['tel'] = $data[0]->telefon;
        $this->arr['opis'] = $data[0]->opis;
        $slika = $data[0]->slika;
        if ($slika == base_url()."img/common/generic.jpg") {
            $slika = "*generic pic* *either no or wrong link* ";
        }
        $this->arr['slika'] = $slika;
        if ($this->users_model->get_role()) {
            $this->arr['page'] = "change_profile_view";
            $this->arr['pol'] = $data[0]->pol;
            $this->arr['godiste'] = $data[0]->datum;
        } else {
            $this->arr['page'] = "change_hala_profile_view";
            $this->arr['adresa'] = $data[0]->adresa;
            $this->arr['gallery'] = $this->gallery_model->get_gallery($data[0]->idK); 
        }
        $this->load->view("templates/page", $this->arr);
    }
    public function update() {
        $username = $this->session->userdata('username');
        if (!$username) {
            redirect(base_url());
            return;
        }
        if(!$this->input->is_ajax_request()){exit('no valid req');}
        $this->load->library('form_validation');
        $this->form_validation->set_rules('mreg','First and Last Name','required');
        $this->form_validation->set_rules('ereg','Email','required|valid_email');
        $this->form_validation->set_rules('breg','Phone number','required|numeric');
        if($this->users_model->get_role()) {
            $this->form_validation->set_rules('dreg','Date','required');
        } else {
            $this->form_validation->set_rules('areg','Adress','required');
        }
        if($this->form_validation->run()) {
            $this->users_model->update_user();
            $this->gallery_model->update_gallery();
            echo '<div class="alert alert-success">success</div>';
        } else {
            echo '<div class="alert alert-danger">'.validation_errors().'</div>';
        }
    }

    public function change_password() {
        $username = $this->session->userdata('username');
        if (!$username) {
            redirect(base_url());
            return;
        }
        $this->arr['page'] = "change_password_profile_view";
        $this->load->view("templates/page", $this->arr);
    }
    
    public function update_password() {
        $username = $this->session->userdata('username');
        if (!$username) {
            redirect(base_url());
            return;
        }
        $data = $this->users_model->get_user($this->session->userdata('username'));
        $pass = $data[0]->password;
        if ($pass == md5($this->input->post('oldpass'))) {
            $newpass = $this->input->post('newpass');
            $ppass = $this->input->post('ppass');
            if(strcmp($newpass, $ppass) == 0) {
                $this->users_model->update_password();
                echo '<div class="alert alert-success">success</div>'; 
            } else {
                echo '<div class="alert alert-danger">Password not confirmed!</div>';
            }
        } else {
            echo '<div class="alert alert-danger">Wrong old password!</div>';
        }
    }
    
    public function rate($target) {
        $username = $this->session->userdata('username');
        if (!$username) {
            redirect(base_url());
            return;
        }
        $this->users_model->rate_user($this->session->userdata('username') ,$target);
        redirect(base_url()."profile/view/".$target);
    }
    public function delete_gallery_image($id) {
        $username = $this->session->userdata('username');
        if (!$username) {
            redirect(base_url());
            return;
        }
        $this->gallery_model->delete_image($id);
        redirect(base_url()."profile/change");
    }
}
?>

