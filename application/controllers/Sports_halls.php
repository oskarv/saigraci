<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once(APPPATH . 'controllers/MyController.php');
class Sports_halls extends MyController {
    
    function __construct() {
        parent::__construct();
        parent::index();
    }

    public function index() {        
          
    }
    
    
    public function getHalls($current){
        $this->arr['page']="Sports_halls_view";
        $from = $current * 6 - 6;
        
        $this->load->model('Sports_halls_model');
        
        $this->arr['q']=$this->Sports_halls_model->getHalls($from);
        $this->arr['from'] = $from;
        $this->arr['current'] = $current;
        $this->load->view("templates/page", $this->arr); 
    }
    
       
}