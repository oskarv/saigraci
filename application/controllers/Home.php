<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once(APPPATH . 'controllers/MyController.php');
class Home extends MyController {
    function __construct(){
        parent::__construct();
        parent::index();      
    }

    public function index() {
        $this->arr['page']="home_view";
        $this->load->model('Home_model');
        $this->arr['q']=$this->Home_model->getFive();
        $this->load->view("templates/page", $this->arr); 
    }
	
	public function login(){
        $is_logged_in=$this->session->userdata('is_logged_in');
        if(isset($is_logged_in) || $is_logged_in==false){
        $q=$this->users_model->validate();
        if($q){
            $data=array(
                'username' =>$this->input->post('username'),
                'is_logged_in'=>true
            );
            $this->session->set_userdata($data);
            echo '<span class="redirect">'.base_url().$this->input->post('page').'</span>';
            return;
        }       
        }
        
         echo '<div class="alert alert-danger">Username or password are incorrect </div>';  
		
}

        public function registerUser(){
         if(!$this->input->is_ajax_request()){exit('no valid req');}
        $this->load->library('form_validation');
        $this->form_validation->set_rules('vrsta','tip','required|callback_check_userhala');
        if(strcmp($this->input->post('vrsta'),'Osoba')==0){
        $this->form_validation->set_rules('ureg','Username','trim|required|min_length[6]|max_length[32]|is_unique[korisnik.username]');
        $this->form_validation->set_rules('preg','Password','trim|required|min_length[4]|max_length[32]');
        $this->form_validation->set_rules('psreg','Password Confirmation','trim|required|matches[preg]');
        $this->form_validation->set_rules('mreg','First and Last Name','required');
        $this->form_validation->set_rules('ereg','Email','required|valid_email|is_unique[korisnik.email]');
        $this->form_validation->set_rules('dreg',"Date",'required|trim|callback_check_valid_date');
        $this->form_validation->set_rules('gender','gender','required|callback_check_gender');
        $this->form_validation->set_rules('breg','Phone number','required|numeric');
        }
        else if(strcmp($this->input->post('vrsta'),'Hala')==0){
        $this->form_validation->set_rules('hureg','Username','trim|required|min_length[6]|max_length[32]|is_unique[korisnik.username]');
        $this->form_validation->set_rules('hpreg','Password','trim|required|min_length[4]|max_length[32]');
        $this->form_validation->set_rules('hpsreg','Password Confirmation','trim|required|matches[hpreg]');
        $this->form_validation->set_rules('hmreg','Name','required');
        $this->form_validation->set_rules('hereg','Email','required|valid_email|is_unique[korisnik.email]');
        $this->form_validation->set_rules('hbreg','Phone number','required|numeric'); 
         $this->form_validation->set_rules('hareg','Adress','required');  
        }
        if($this->form_validation->run()==false)
        echo '<div class="alert alert-danger">'.validation_errors().'</div>';
        else if(strcmp($this->input->post('vrsta'),'Osoba')==0){
        if($q=$this->users_model->create_user())
            echo '<div class="alert alert-success"> You are successful registered!</div>';
        else 
            $this->index();
        }
        else if(strcmp($this->input->post('vrsta'),'Hala')==0){
        if($q=$this->users_model->create_hala())
            echo '<div class="alert alert-success"> You are successful registered!</div>';
        else 
            $this->index();
        }
        }
        
        
        public function registerHala(){  

           if($this->form_validation->run()==false)
                echo '<div class="alert alert-danger">'.validation_errors().'</div>';
           else if(strcmp($this->input->post('vrsta'),'Osoba')==0)     
                if($q=$this->users_model->create_user())
                        echo '<div class="alert alert-success"> You are successful registered!</div>'; 
                else
                        $this->index();
     
            else if(strcmp($this->input->post('vrsta'),'Hala')==0)
                if($q=$this->users_model->create_hala())
                        echo '<div class="alert alert-success"> You are successful registered!</div>'; 
                else
                        $this->index();
       $this->index();
        }
        
        public function logout(){
       $is_logged_in=$this->session->userdata('is_logged_in');
        if($is_logged_in==true){
            $this->session->unset_userdata('is_logged_in');
            $this->session->sess_destroy();
        }
            redirect($this->input->post('page'));
        }
        
        public function check_gender($str){
            if(strcmp($str,"male")==0 || strcmp($str,'female')==0)
            return true;
            $this->form_validation->set_message('check_gender', 'Please select male or female gender');
            return false;
        }
        
        public function check_valid_date($date){
        if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$date))
        return true;
        $this->form_validation->set_message('check_valid_date', 'Date must be in yyyy-mm-dd format');
        return false;
        }
        
       public function check_userhala($str){
            if(strcmp($str,"Osoba")==0 || strcmp($str,'Hala')==0)
            return true;
            $this->form_validation->set_message('check_userhala', 'Please select Osoba ili Hala');
            return false;
        }
       
}

