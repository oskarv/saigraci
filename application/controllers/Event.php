<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once(APPPATH . 'controllers/MyController.php');
class Event extends MyController {
    
    function __construct() {
        parent::__construct();
        parent::index();
	}
	
	public function index(){

            $this->arr['reklame'] = false; // da nema reklama
		if ($this->arr['is_logged_in']==true) { 
			$this->arr['page']="my_events_view";
			$this->load->model('EventModel');
			$this->arr['q'] = $this->EventModel->get_my_events();	
			$this->arr['page']="my_events_view";
		}
		else{
			redirect(base_url());
		}
		
		$this->load->view("templates/page", $this->arr);

	}
	
	public function dogadjaj($dog){

		if ($this->arr['is_logged_in']==true)
			$this->arr['button'] = true;
		else
			$this->arr['button'] = false;

		$num = 1;
		$this->arr['num'] = $num;
		
		$this->arr['page']="event_view";
		$this->load->model('EventModel');
		$this->arr['rezultat'] = $this->EventModel->get_event($dog);	
		$this->arr['page']="event_view";
		
		$this->arr['rezultat1'] = $this->EventModel->get_prijavljeni($dog);
		
		//komentari
		
		$this->arr['eventNum'] = $dog;
		
		$this->arr['komentari'] = $this->EventModel->getComments($dog, 1);
		$this->arr['br_kom'] = $this->EventModel->getNumComments($dog);

		//komentari
		
		
		$this->arr['eventNum'] = $dog;

		if ($this->arr['is_logged_in']==true){
			$this->arr['rezultat2'] = $this->EventModel->is_registered_for_event($dog);
		
			$username = $this->session->userdata('username');
			$this->db->where('username', $username);
			$q = $this->db->get('Korisnik')->row();		
			$this->arr['rezultat4'] = $q;			
		}
		
		//$this->EventModel->setEventNum($dog);
		
		$this->load->view("templates/page", $this->arr);
	}
	
	public function dogadjajtermini(){
		 if(!$this->input->is_ajax_request()){exit('no valid req');}
		 $this->load->model('EventModel');
		 $rezultat = $this->EventModel->find_date();	
		 $broj = 1;
		 
		 if  ($this->input->post('dreg')){
		 foreach($rezultat as $row){
			echo ' <tr>
						<td>'.$broj.'</td>
											<td><a href="'.base_url().'profile/view/'.$row->husername.'">'.$row->husername.'</a></td>
											<td>'.$row->datumOd.'</td>
											<td>'.$row->cena.'</td>
											<td style="text-align: center">
												<input type="radio" form = "forma" name = "radioHala" value="'.$row->idT.'">
											</td>
					</tr> ';
					$broj++;
		 }
		 }
	}
	
	public function create_menu (){
		if ($this->arr['is_logged_in']==true) {
			$this->arr['page']="add_event_view";
			$this->load->view("templates/page", $this->arr);
		}
		else{
				redirect(base_url());
		}
	}
	
	public function create (){
		if ($this->arr['is_logged_in']==true) {
			$this->arr['page']="add_event_view";
			$napravi = 0;
			
			if($this->input->post('radioHala')){
			$this->load->model('EventModel');
			$napravi = $this->EventModel->create();
			}
			
			redirect(base_url()."event");
		}
		else{
			redirect(base_url());
		}			
	}
	
	public function delete_ev($dog){	
		if ($this->arr['is_logged_in']==true) {
			$this->load->model('EventModel');		
			$data = $this->EventModel->delete_event($dog);
			
		}
		else{
			redirect(base_url());
		}	
		
		redirect(base_url()."event");
	}
	
	public function odbij_kor(){	
		if ($this->arr['is_logged_in']==true) {
			$this->load->model('EventModel');		
			$data = $this->EventModel->odbij_korisnika($this->input->get('user'), $this->input->get('dog'));
		}
		else{
			redirect(base_url());
		}	
		
		redirect(base_url()."event/prijavljeni");
	}
	
	public function prihvati_kor(){	
		if ($this->arr['is_logged_in']==true) {
			$this->load->model('EventModel');		
			$data = $this->EventModel->prihvati_korisnika($this->input->get('user'), $this->input->get('dog'));
		}
		else{
			redirect(base_url());
		}	
		
		redirect(base_url()."event/prijavljeni");
	}
	
	public function register_for_ev(){

		if ($this->arr['is_logged_in']==true) {
		
			$username = $this->session->userdata('username');
			$this->db->where('username', $username);
			$q = $this->db->get('korisnik');
			foreach ($q->result() as $row) {
				$idKor = $row->idK;
			}
		
			$this->load->model('EventModel');		
			$data = $this->EventModel->register_for_event($idKor, $this->input->get('dog'));
			redirect(base_url()."event/dogadjaj/".$this->input->get('dog'));
		}		
		redirect(base_url());	
	}
	
	public function prijavljeni(){	
	if ($this->arr['is_logged_in']==true) {
			$this->arr['reklame'] = false;
			$this->arr['page']="registered_for_my_events_view";
			$this->load->model('EventModel');
			$this->arr['rezultat'] = $this->EventModel->get_prijavljeni_my_events();	
		
			$this->load->view("templates/page", $this->arr);
		}
		else{
			redirect(base_url());
		}
	}

	public function addComment($dog){
		 $tekst = $this->input->post('comment');
		 $tekst = trim($tekst);
		if(strlen($tekst) > 0){
			$this->load->model('EventModel');
			$this->EventModel->addComm($dog);
		}
	}
	
	public function delComment($id){
		$username = $this->session->userdata('username');
		$this->db->where('username', $username);
		$q = $this->db->get('Korisnik');
		foreach ($q->result() as $row) {
			$idK = $row->idK;
		}
		$this->load->model('EventModel');
		$this->EventModel->delComment($id, $idK);
	}

	public function comments($dog, $num){
                if(!$this->input->is_ajax_request()){exit('no valid req');}
				
                $this->load->model('eventmodel');
                $comm = $this->eventmodel->getComments($dog, $num);
                $num_of_comments = $this->eventmodel->getNumComments($dog, $num);
				$array = array(
					'num_of_comments' => $num_of_comments,
					'output' => ""
				);
				
				if ($this->arr['is_logged_in']==true){
					$username = $this->session->userdata('username');
					$this->db->where('username', $username);
					$q = $this->db->get('Korisnik');
					foreach ($q->result() as $row) {
						$id = $row->idK;
					}
				}
				
                    foreach ($comm as $row) {
						$array['output'] .= '
						<li class="comment">
							<p class="comment-author" style="height : 50px">
										<img width="48" height="48" class="avatar avatar-48 photo" src="'.$row->slika.'" alt="" />
							<cite><a class="url" rel="external nofollow" href="'.base_url().'profile/view/'.$row->username.'">'.$row->username.'</a></cite>
							<br><small class="comment-time">'.$row->datum.'</small>
									</p>';
									if ($this->arr['is_logged_in']==true)
									if($id==$row->idK) $array['output'] .='
									<input type="button" onclick="deleteComment('.$row->idKom.')" style="float:right" class="btn" value="Obrisi" name="submit">
									';
									$array['output'] .='
									<div class="commententry">
										<p>'.nl2br($row->tekst).'</p>
							</div>
						</li>
								';
					} 
				echo json_encode($array);
				
                $num++;
				
	}
}
