<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MyController extends CI_Controller {


    function __construct()
    {
        parent::__construct();
         $this->load->model('users_model');
         $this->load->model('gallery_model');
    }
    public $arr=array();
    
    
    public function index() {
     $is_logged_in=$this->session->userdata('is_logged_in');
     $username=$this->session->userdata('username');
      if(!isset($is_logged_in) || $is_logged_in!=true){
        $this->arr = array(
            "title" => "Pronadji Saigrace", 
			"header" => "templates/headers/header_guest",
			"is_logged_in"=>false
        );
            }
       else{
           
           if($this->users_model->get_role()){
        $this->arr = array(
            "title" => "Hi | ".$username, 
            "username"=>$username,
			"header" => "templates/headers/header_user",
			"is_logged_in"=>true
        );
            }
            else{
            $this->arr = array(
            "title" => "Hi | ".$username, 
            "username"=>$username,
			"header" => "templates/headers/header_hala",
			"is_logged_in"=>true
        ); 
            }
            
       }
       $this->arr['side'] = "templates/side";
       $this->arr['reklame'] = true;
       $this->arr['search'] = "templates/search";
    }

}


