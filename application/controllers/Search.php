<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once(APPPATH . 'controllers/MyController.php');
class Search extends MyController {
    
    function __construct() {
        parent::__construct();
        parent::index();
    }

    public function index() {
          
    }
    
    
    public function pretraga($current){
        $this->arr['page']="Search_view";
        $this->arr['reklame'] = false; // da nema reklama
        $from = $current * 3 - 3;
        $this->load->model('Search_model');
        $this->arr['q']=$this->Search_model->getValue($from);
        $this->arr['from'] = $from;
        $this->arr['current'] = $current;
        $this->load->view("templates/page", $this->arr); 
    }
    
       
}