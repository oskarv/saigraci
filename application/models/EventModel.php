<?php

class EventModel extends CI_Model {

	function find_date(){
	
		$y = $this->input->post('dreg');
		$this->db->select('*, hala.username as husername');
		$this->db->join('korisnik as hala', 'hala.idK = termin.idK');
		
		if($y){
				$this->db->where('datumOd >=', $y);
				return $this->db->get('termin')->result();
				}	
	}


	function create(){

	    $username = $this->session->userdata('username');
        $this->db->where('username', $username);
        $q = $this->db->get('Korisnik');
        foreach ($q->result() as $row) {
            $id = $row->idK;
        }
		
		$termin = $this->input->post('radioHala');
        $this->db->where('idT', $termin);		
        $q = $this->db->get('termin');
        foreach ($q->result() as $row) {
            $idH = $row->idK;
        }
		
		$grad = 'Beograd';
		$flag = 0;
		
		$idDog = $this->db->select('idD')->order_by('idD','desc')->limit(1)->get('dogadjaj')->row('idD');
		$idDog = $idDog + 1;
	
	 $new_event=array(
			'idD' => $idDog,
            'opis' => $this->input->post('komentar'),
            'naslov' => $this->input->post('nazivdog'),
            'slika' => $this->input->post('slika'),
            'idKO' => $id,
            'idKH' => $idH,
            'idT' => $termin,
			'pol' => $this->input->post('gender'),
            'godine_od' => $this->input->post('godine_od'),
            'godine_do' => $this->input->post('godine_do'),
            'sport' => $this->input->post('sport'),
            'grad' => $grad,
			'flagBrisanje' => $flag
        );
		
        $insert=$this->db->insert('dogadjaj',$new_event);
		
		return $insert;
	
	}

	function get_my_events(){
	    $username = $this->session->userdata('username');
        $this->db->where('username', $username);
        $q = $this->db->get('Korisnik');
        foreach ($q->result() as $row) {
            $id = $row->idK;
        }
		$flag = 0;
	
		$this->db->select('*, dogadjaj.opis as dogOpis, hala.username as husername, dogadjaj.slika as dogSlika, hala.ime as hime');
            $this->db->limit(6);
			$this->db->from('dogadjaj');
			$this->db->order_by('termin.datumOd', 'desc');
			$this->db->where('flagBrisanje', $flag);
			$this->db->where('idKO', $id);
			$this->db->join('korisnik as hala', 'hala.idK = dogadjaj.idKH');
			$this->db->join('korisnik as osoba', 'osoba.idK = dogadjaj.idKO');
			$this->db->join('termin', 'termin.idT = dogadjaj.idT');
                        
			return  array(
				'result' => $this->db->get()->result()
			);	
	}
	
	function get_event($dog) {

	$this->db->where('idD', $dog);
	$q = $this->db->get('dogadjaj')->row();
			
	return $q;
    }
	
	function delete_event($dog) {
		$q = 1;		
		$data = array('flagBrisanje' => $q);
        $this->db->where('idD', $dog);
        $this->db->update('dogadjaj', $data);
    }
	
	function odbij_korisnika($user, $dog) {
		$q = "odbijen";		
		$data = array('status' => $q);
        $this->db->where('idK', $user);
		$this->db->where('idD', $dog);
        $this->db->update('prijavljen', $data);
    }
	
	function prihvati_korisnika($user, $dog) {
		$q = "prihvacen";		
		$data = array('status' => $q);
        $this->db->where('idK', $user);
		$this->db->where('idD', $dog);
        $this->db->update('prijavljen', $data);
    }
	
	function register_for_event($user, $dog){
	
		$status = "na cekanju";
		
		$new = array(
			'idD' => $dog,
            'idK' => $user,
			'status' => $status
        );
		
        $this->db->insert('prijavljen',$new);	
	}
	
	function get_prijavljeni($dog) {
	
		$this->db->where('idD', $dog);
		$this->db->join('korisnik', 'korisnik.idK = prijavljen.idK');
		$this->db->join('osoba', 'osoba.idK = prijavljen.idK');
		$q = $this->db->get('prijavljen')->result();
			
		return $q;
		
	}
	
	function is_registered_for_event($dog) {
	
		
		$username = $this->session->userdata('username');
		$this->db->where('username', $username);
		$q = $this->db->get('Korisnik');
		foreach ($q->result() as $row) {
			$id = $row->idK;
		}
	
		$this->db->where('idD', $dog);
		$this->db->where('idK', $id);
		$q = $this->db->get('prijavljen')->result();
			
		return $q;
		
	}
	
	function get_prijavljeni_my_events() {
		
		$username = $this->session->userdata('username');
		$this->db->where('username', $username);
		$q = $this->db->get('Korisnik');
		foreach ($q->result() as $row) {
			$id = $row->idK;
		}
	
		$flag = 0;
		$this->db->where('idKO', $id);
		$this->db->where('flagBrisanje', $flag);
		$this->db->join('prijavljen', 'prijavljen.idD = dogadjaj.idD');
		$this->db->join('korisnik', 'korisnik.idK = prijavljen.idK');
		$q = $this->db->get('dogadjaj')->result();
			
		return $q;
		
	}
	
	function getComments($dog, $num){

		$this->db->select('*');
		$this->db->from('komentar');
		$this->db->order_by('komentar.datum', 'desc');
		$this->db->where('idD', $dog);
		$this->db->limit(5*$num);
		$this->db->join('korisnik', 'korisnik.idK = komentar.idK');

		return $this->db->get()->result();

	}
        function getNumComments($dog){

		$this->db->select('*');
		$this->db->from('komentar');
		//$this->db->order_by('komentar.datum', 'desc');
		$this->db->where('idD', $dog);
		//$this->db->limit(5*$num);
		$this->db->join('korisnik', 'korisnik.idK = komentar.idK');
        
		return $this->db->count_all_results();

	}

	function setEventNum($dog){
		echo '<input type="hidden" name="eventNum" value="'.$dog.'">';
	}

	function getEventNum(){
		return $this->input->post('eventNum');
	}

	function addComm($dog){
		
		$username = $this->session->userdata('username');
        $this->db->where('username', $username);
        $q = $this->db->get('Korisnik');
        foreach ($q->result() as $row) {
            $id = $row->idK;
        }

        $tekst = $this->input->post('comment');
        $datum = date("Y-m-d H:i:s");
	
	 $new_comm=array(
            'tekst' => $tekst,
            'datum' => $datum,
            'idK' => $id,
            'idD' => $dog
        );

	if($tekst != "")
		$this->db->insert('komentar', $new_comm);

	}
	
	function delComment($id, $idK){
		$this->db->where('idKom', $id);		
		$this->db->where('idK', $idK);
        $this->db->delete('komentar');
	}


}


?>