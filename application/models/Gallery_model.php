<?php

class Gallery_model extends CI_Model {
    
    public function get_gallery($id) {
        $this->db->where('idK', $id);
        $q = $this->db->get('galerija');
        return $q->result();
    }
    
    public function update_gallery() {
        $username = $this->session->userdata('username');;
        $q = $this->users_model->get_user($username);
        $id = $q[0]->idK;
        $idG = $this->db->select('idG')->order_by('idG','desc')->limit(1)->get('galerija')->row('idG');  echo $idG;
        
        $galerija = $this->input->post('gal');
        if (!$galerija) return;
        foreach ($galerija as $row) {
            if (@getimagesize($row)) {
                $this->db->where('slika', $row);
                $this->db->where('idK', $id);
                $q = $this->db->get('galerija');
                
                if (!$q->row()) {
                    $idG = $idG + 1;
                    $data = array (
                        'idK' => $id,
                        'idG' => $idG,
                        'slika' => $row
                    );
                    $this->db->insert('galerija', $data);
                }
            }
        }
    }
    
    public function delete_image($id) {
        $this->db->where('idG', $id);
        $this->db->delete('galerija');
    }
}
