<?php

class Term_model extends CI_Model {
    

function getAllTerms(){
    
    $username = $this->session->userdata('username');
    $this->db->where('username', $username);
    $q = $this->db->get('Korisnik');
    foreach ($q->result() as $row) {
        $id = $row->idK;
    }
    
    $this->db->select('*');
    $this->db->from('termin');
    $this->db->where('idK', $id);
    
    return $this->db->get()->result();
    
}


function create(){

	    $username = $this->session->userdata('username');
        $this->db->where('username', $username);
        $q = $this->db->get('Korisnik');
        foreach ($q->result() as $row) {
            $id = $row->idK;
        }

        $datumOd = $this->input->post('datumOd');
        $datumDo = $this->input->post('datumDo');
        $cena = $this->input->post('cena');
	
	 $new_term=array(
            'idK' => $id,
            'datumOd' => $datumOd,
            'datumDo' => $datumDo,
            'cena' => $cena
        );
		
        if(($cena != "") && ($cena >= 0) && ($datumOd != null) && ($datumDo != null) && ($datumOd < $datumDo)){
            $this->db->insert('termin', $new_term);
            return "SUCCESS";
        }else{
            $ret = "ERROR!";
            if($datumOd == null) $ret .= "<br/>Datum od - prazno";
            if($datumDo == null) $ret .= "<br/>Datum do - prazno";
            if($datumOd > $datumDo) $ret .= "<br/>Datum od > Datum do";
            if($cena == "") $ret .= "<br/>Cena - prazno";
            if($cena < 0) $ret .= "<br/>Cena < 0";
            return $ret;
        }
	
	}
    
    function delTerm($id){
		$this->db->where('idT', $id);	
        $this->db->delete('termin');
    }
}
?>