<?php

class Home_model extends CI_Model{    
	
    function getFive(){		
		
			$this->db->select('*, dogadjaj.opis as dogOpis, hala.username as husername, dogadjaj.slika as dogSlika, hala.ime as hime');
            $this->db->limit(6);
			$this->db->from('dogadjaj');
			$this->db->order_by('termin.datumOd', 'desc');
			$this->db->where('flagBrisanje', 0);
			$this->db->join('korisnik as hala', 'hala.idK = dogadjaj.idKH');
			$this->db->join('korisnik as osoba', 'osoba.idK = dogadjaj.idKO');
			$this->db->join('termin', 'termin.idT = dogadjaj.idT');
                        
			return  array(
				'result' => $this->db->get()->result()
			);	
			
    }

}