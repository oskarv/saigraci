<?php

class Search_model extends CI_Model{
	
	public $sports = array('Svi', 'Fudbal', 'Kosarka', 'Hokej', 'Ragbi');
	public $cities = array('Svi', 'Beograd', 'Krupanj', 'Zajecar', 'Sremska Mitrovica', 'Bor');
	public $pol = array('-', 'M', 'Z');
    
		
	
	function funcSlect() {
				$y = array(
						'sport' => $this->input->get('sport'),
						'grad' => $this->input->get('grad'),
						'datumOd' => $this->input->get('datumOd'),
						'datumDo' => $this->input->get('datumDo'),
						'pol' => $this->input->get('pol'),
						'godineOd' => $this->input->get('godineOd'),
						'godineDo' => $this->input->get('godineDo')
					);
				$this->db->select('*, dogadjaj.opis as dogOpis, hala.username as husername, dogadjaj.slika as dogSlika, hala.ime as hime');
				$this->db->from('dogadjaj');
				if($y['sport'] != "Svi")
					$this->db->where('sport', $y['sport']);
				if($y['grad'] != "Svi")
					$this->db->where('grad', $y['grad']);
				if($y['datumOd'])
					$this->db->where('termin.datumOd >=', $y['datumOd']);			
				if($y['datumDo'])
					$this->db->where('termin.datumDo <=', $y['datumDo']);
				if($y['pol'] != "-")
					$this->db->where('pol', $y['pol']);			
				if($y['godineOd'] != "")	
					$this->db->where('godine_od >=', $y['godineOd']);
				if($y['godineDo'] != "")	
					$this->db->where('godine_do <=', $y['godineDo']);	
				$this->db->where('flagBrisanje', 0);
				$this->db->join('korisnik as hala', 'hala.idK = dogadjaj.idKH');
				$this->db->join('korisnik as osoba', 'osoba.idK = dogadjaj.idKO');
				$this->db->join('termin', 'termin.idT = dogadjaj.idT');
			}

    function getValue($from){
					
			$this->funcSlect();
			$w = $this->db->get();
			$this->funcSlect();
			$this->db->limit(3, $from);
			$q=$this->db->get();
			return  array(
				'numOfRows' => $w->num_rows(),
				'result' => $q->result(),
				'sports' => $this->sports,
				'cities' => $this->cities
			);

	}
	
	
}