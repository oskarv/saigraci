<?php

class Users_model extends CI_Model {

    function validate() {
        $this->db->where('username', $this->input->post('username'));
        $this->db->where('password', md5($this->input->post('password')));
        $q = $this->db->get('Korisnik');
        if ($q->num_rows() == 1) {
            return true;
        }
    }

    function get_role() { // proverava da li je hala ili osoba ulogovana
        $username = $this->session->userdata('username');
        $this->db->where('username', $username);
        $q = $this->db->get('Korisnik');
        foreach ($q->result() as $row) {
            $id = $row->idK;
        }
        $this->db->where('idK', $id);
        $q = $this->db->get('osoba');
        if ($q->num_rows() == 1) {
            return true;
        } else {
            return false;
        }    }

    function get_role_user($id) { // vraca sta je na osnovu id
        $this->db->where('idK', $id);
        $q = $this->db->get('osoba');
        if ($q->num_rows() == 1) {
            return true;
        } else {
            return false;
        }
    }

    function get_user($username) { // vraca sve podatke o zadatom korisniku
        $this->db->where('username', $username);
        $query = $this->db->get('Korisnik');
        foreach ($query->result() as $row) {
            $id = $row->idK;
        }
        if ($this->users_model->get_role_user($id)) {
            $this->db->where('Osoba.idK', $id);
            $this->db->where('Korisnik.idK', $id);
            $this->db->from('Osoba, Korisnik');
        } else {
            $this->db->where('Hala.idK', $id);
            $this->db->where('Korisnik.idK', $id);
            $this->db->from('Hala, Korisnik');
        }
        return $this->db->get()->result();
    }
    
 function create_user(){
        $new_user_data=array(
            'username' => $this->input->post('ureg'),
            'password' => md5($this->input->post('preg')),
            'email' => $this->input->post('ereg'),
            'ime' => $this->input->post('mreg'),
            'telefon' =>$this->input->post('breg'),
            'slika' => base_url()."img/common/generic.jpg"
        );
        $insert=$this->db->insert('korisnik',$new_user_data);
        if($insert){
            $this->db->where('username',$this->input->post('ureg'));
            $q=$this->db->get('korisnik')->row()->idK;
            
        $pol="";
        if(strcmp($this->input->post('gender'),"male")==0){
            $pol='m';
        }
        else $pol='f';
        $new_person_data=array(
          'idK' => $q,
          'pol' => $pol,
          'datum' =>  $this->input->post('dreg')
        );
        
       $insert=$this->db->insert('osoba',$new_person_data);
        return $insert;
    }
    return $insert;
}

 function create_hala(){
        $new_user_data=array(
            'username' => $this->input->post('hureg'),
            'password' => md5($this->input->post('hpreg')),
            'email' => $this->input->post('hereg'),
            'ime' => $this->input->post('hmreg'),
            'telefon' =>$this->input->post('hbreg'),
            'slika' => base_url()."img/common/generic.jpg"
        );
        $insert=$this->db->insert('korisnik',$new_user_data);
        if($insert){
            $this->db->where('username',$this->input->post('hureg'));
            $q=$this->db->get('korisnik')->row()->idK;
        $new_hala_data=array(
          'idK' => $q,
          'adresa' =>$this->input->post('hareg')
        );
       $insert=$this->db->insert('hala',$new_hala_data);
        return $insert;
    }
    return $insert;
}

    function update_user() {
        $username = $this->session->userdata('username');
        $slika = $this->input->post('sreg');
        if (!@getimagesize($slika)) {
            $slika = base_url()."img/common/generic.jpg";
        }
        $data = array(
                'ime' => $this->input->post('mreg'),
                'email' => $this->input->post('ereg'),
                'telefon' => $this->input->post('breg'),
                'opis' => $this->input->post('opis'),
                'slika' => $slika
        );
        $this->db->where('username', $username);
        $this->db->update('korisnik', $data);

        $this->db->where('username', $username);
        $query = $this->db->get('korisnik');
        foreach ($query->result() as $row) {
            $id = $row->idK;
        }

        if ($this->users_model->get_role()) {
            $pol="";
            if(strcmp($this->input->post('gender'),"male")==0){
                $pol='m';
            }
            else $pol='f';
            $newdata = array (
                'pol' => $pol,
                'datum' => $this->input->post('dreg')
            );
            $this->db->where('idK', $id);
            $this->db->update('osoba', $newdata);
        } else {
            $newdata = array (
                'adresa' => $this->input->post('areg')
            );
            $this->db->where('idK', $id);
            $this->db->update('hala', $newdata);
        }
        
    }
    function update_password() {
        $newpass = $this->input->post('newpass');
        $ppass = $this->input->post('ppass');
        $data = array(
            'password' => md5($newpass)
        );
        $this->db->where('username', $this->session->userdata('username'));
        $this->db->update('korisnik', $data);
    }
    
    function rate_user($ocenjivac, $ocenjivan) {
        $rating = $this->input->post('ocena');
        if (!$rating) return;
        
        $this->db->where('username', $ocenjivac);
        $q = $this->db->get('korisnik');
        $id_ocenjivac = $q->row()->idK;
        $this->db->where('username', $ocenjivan);
        $q = $this->db->get('korisnik');
        $id_ocenjivan = $q->row()->idK;
        
        $this->db->where('idK', $id_ocenjivan);
        $this->db->where('idK_ocenjivac', $id_ocenjivac);
        $q = $this->db->get('ocena');
        
        if ($q->row() != null) { // update
            $this->db->where('idK', $id_ocenjivan);
            $this->db->where('idK_ocenjivac', $id_ocenjivac);
            $arr = array (
                'ocena' => $rating
            );
            $this->db->update('ocena', $arr);
        } else {          // create
            $arr = array (
                'idK' => $id_ocenjivan,
                'idK_ocenjivac' => $id_ocenjivac,
                'ocena' => $rating
            );
            $this->db->insert('ocena', $arr);
        }
        
    }
    function get_ocena($id) {
        $this->db->where('idK', $id);
        $q = $this->db->get('ocena');
        if (!$q->row()) return '-';
        $sum = 0; $n = 0;
        foreach($q->result() as $row) {
            $sum += $row->ocena;
            $n++;
        }
        return $sum / $n;
    }
    function get_br_glasova($id) {
        $this->db->where('idK', $id);
        $q = $this->db->get('ocena');
        return $q->num_rows();
    }
}
