-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 01, 2016 at 12:44 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `psi`
--

-- --------------------------------------------------------

--
-- Table structure for table `dogadjaj`
--

CREATE TABLE `dogadjaj` (
  `idD` int(11) NOT NULL,
  `opis` varchar(255) DEFAULT NULL,
  `naslov` varchar(45) DEFAULT NULL,
  `slika` varchar(120) DEFAULT NULL,
  `idKO` int(11) NOT NULL,
  `idKH` int(11) NOT NULL,
  `idT` int(11) NOT NULL,
  `pol` varchar(1) DEFAULT NULL,
  `godine_od` int(11) DEFAULT NULL,
  `godine_do` int(11) DEFAULT NULL,
  `grad` varchar(45) DEFAULT NULL,
  `sport` varchar(45) NOT NULL,
  `flagBrisanje` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dogadjaj`
--

INSERT INTO `dogadjaj` (`idD`, `opis`, `naslov`, `slika`, `idKO`, `idKH`, `idT`, `pol`, `godine_od`, `godine_do`, `grad`, `sport`, `flagBrisanje`) VALUES
(0, 'ponesi opremu!', 'Ragbi turnir', 'http://iluvesports.com/wp-content/uploads/2015/02/40-Funny-American-Football-Quotes1.jpg', 14, 7, 2, 'm', 20, 25, 'Beograd', 'Ragbi', 1),
(4, '', 'Odbojka', 'http://sportklub.rs/vesti/74822/126359137.jpg', 14, 6, 1, 'f', 18, 29, 'Beograd', 'Odbojka', 0),
(5, '', 'Kvidic turnir', 'https://i.ytimg.com/vi/YOVS9yn2R7c/maxresdefault.jpg', 14, 6, 1, 'f', 18, 29, 'Beograd', 'Kvidic', 1),
(6, 'imamo loptu', 'Kosarka 3 na 3', 'https://blog-blogmediainc.netdna-ssl.com/SportsBlogcom/filewarehouse/68364/cb5f3400e00bcd0a0324ae0ff4e43542.jpg', 15, 16, 3, 'm', 25, 35, 'Beograd', 'kosarka', 0),
(7, '', 'test', 'url', 15, 16, 3, 'n', 0, 0, 'Beograd', 'Sport', 0);

-- --------------------------------------------------------

--
-- Table structure for table `galerija`
--

CREATE TABLE `galerija` (
  `idG` int(11) NOT NULL,
  `idK` int(11) NOT NULL,
  `slika` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `hala`
--

CREATE TABLE `hala` (
  `idK` int(11) NOT NULL,
  `adresa` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `hala`
--

INSERT INTO `hala` (`idK`, `adresa`) VALUES
(6, NULL),
(7, NULL),
(9, NULL),
(10, 'opet neka'),
(13, 'asdfasdfasdf'),
(16, 'bb');

-- --------------------------------------------------------

--
-- Table structure for table `komentar`
--

CREATE TABLE `komentar` (
  `idKom` int(11) NOT NULL,
  `tekst` varchar(255) DEFAULT NULL,
  `datum` datetime DEFAULT NULL,
  `idK` int(11) NOT NULL,
  `idD` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `komentar`
--

INSERT INTO `komentar` (`idKom`, `tekst`, `datum`, `idK`, `idD`) VALUES
(1, 'Da li imate sudiju?', '2016-05-29 18:18:25', 15, 6);

-- --------------------------------------------------------

--
-- Table structure for table `korisnik`
--

CREATE TABLE `korisnik` (
  `idK` int(11) NOT NULL,
  `username` varchar(45) NOT NULL,
  `password` varchar(32) NOT NULL,
  `email` varchar(45) DEFAULT NULL,
  `ime` varchar(45) DEFAULT NULL,
  `opis` varchar(255) DEFAULT NULL,
  `telefon` varchar(15) DEFAULT NULL,
  `slika` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `korisnik`
--

INSERT INTO `korisnik` (`idK`, `username`, `password`, `email`, `ime`, `opis`, `telefon`, `slika`) VALUES
(1, 'stefan', '5e8380b694734129e89f9c247a0a5cd0', 'ss', 'Stefan', 'Cao Ovde Stefan', '0633182385', ''),
(2, 'petar', '5b203658f1b0da1596db00ba59ee753f', 'ss', 'petar', NULL, NULL, ''),
(3, 'ivanpp', '5e8380b694734129e89f9c247a0a5cd0', 'ivan@gmail.com', 'ivan pet', NULL, '0612345221', ''),
(4, 'vidoje', 'bc300b96f9a3572286968d7336338ef4', 'zeljicvidoje@gmail.com', 'Vidoje Zeljic', NULL, '063630070', ''),
(5, 'dusanzeljic', 'de2096cf5a6889a074ceac14cd9bae91', 'zeljicdusan@gmail.com', 'Zeljic Dusan', NULL, '0653778830', ''),
(6, 'halasportova', 'd0507c30e72c6e9e00a6d9fc0bff38fd', 'hala@gmail.com', 'Hala Sportova', NULL, '061234567', ''),
(7, 'pionir', '7dfbdffe7a6459530524706b86f0232f', 'pionir@gmail.com', 'Hala Pionir', NULL, '063123456', ''),
(8, 'Stefan32', '2e970e822e1a8834203d06abb60f59ec', 'stekos@live.com', 'Stefan Kostic', 'Pozdrav svima, ovde sam ja i niko vise :D', '12345', 'http://zblogged.com/wp-content/uploads/2015/11/17.jpg'),
(9, 'HalaNeka', '5b203658f1b0da1596db00ba59ee753f', 'sport@sala.com', 'Sport sala', NULL, '0619014401', ''),
(10, 'Stefanhala', '2e970e822e1a8834203d06abb60f59ec', 'hala@hala.com', 'Hala Stefan', 'Ovo je nova hala najbolja!', '893741', 'http://localhost:1234/img/common/generic.jpg'),
(11, 'Stefan22', '2e970e822e1a8834203d06abb60f59ec', 'stefan@stefan.stefan', 'stefan Kostic', '', '213421412', ''),
(12, 'Covekc', 'c2ecdb69d5bce75597101fd6d4f5babe', 'covek@covek.covek', 'covek', '', '234234', 'http://localhost:1234/img/common/generic.jpg'),
(13, 'sdfsdf', 'd58e3582afa99040e27b92b13c8f2280', 'sdfsdf@sdf.sdf', 'sdf', '', '12312421', 'http://www.betstudy.com/stadium/5/5356.jpg'),
(14, 'mikica', '6bba39c8d796c3506ae0e329c358d250', 'mik@osoba.com', 'Nemanja Mikic', NULL, '0658899887', 'http://localhost:1234/img/common/generic.jpg'),
(15, 'oskarv', '6753f5921097c281d8baf98a20c00db1', 'osk@live.com', 'oskar v', NULL, '0588', 'http://localhost:1234/img/common/generic.jpg'),
(16, 'hala123', '8a4a5bde22c54162d0e4c0b8e5f37c35', 'miki@hale.com', 'Hala Mikic', NULL, '099', 'http://localhost:1234/img/common/generic.jpg'),
(17, 'vidojez', 'cc80e4a9c9e4c6ef9ed1852683d41521', 'vid@osoba.com', 'Vidoje Zeljic', NULL, '06777', 'http://localhost:1234/img/common/generic.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `ocena`
--

CREATE TABLE `ocena` (
  `idK` int(11) NOT NULL,
  `idK_ocenjivac` int(11) NOT NULL,
  `idO` int(11) NOT NULL,
  `ocena` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ocena`
--

INSERT INTO `ocena` (`idK`, `idK_ocenjivac`, `idO`, `ocena`) VALUES
(2, 8, 6, 3),
(2, 11, 7, 5),
(8, 11, 8, 5);

-- --------------------------------------------------------

--
-- Table structure for table `osoba`
--

CREATE TABLE `osoba` (
  `idK` int(11) NOT NULL,
  `datum` date DEFAULT NULL,
  `pol` varchar(1) DEFAULT NULL,
  `admin` int(1) DEFAULT NULL,
  `ocena` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `osoba`
--

INSERT INTO `osoba` (`idK`, `datum`, `pol`, `admin`, `ocena`) VALUES
(1, '0000-00-00', 'm', 0, 0.4),
(2, '0000-00-00', 'm', 0, NULL),
(3, '0000-00-00', 'm', NULL, NULL),
(4, '1994-07-13', 'm', NULL, NULL),
(5, '1992-12-25', 'm', NULL, NULL),
(8, '1990-11-07', 'm', NULL, NULL),
(11, '2000-05-24', 'm', NULL, NULL),
(12, '1112-12-22', 'm', NULL, NULL),
(14, '2000-01-01', 'm', NULL, NULL),
(15, '2016-01-02', 'm', NULL, NULL),
(17, '2016-01-01', 'm', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `prijavljen`
--

CREATE TABLE `prijavljen` (
  `idP` int(11) NOT NULL,
  `idD` int(11) NOT NULL,
  `idK` int(11) NOT NULL,
  `status` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `prijavljen`
--

INSERT INTO `prijavljen` (`idP`, `idD`, `idK`, `status`) VALUES
(17, 6, 15, 'odbijen'),
(18, 4, 15, 'prihvacen'),
(19, 0, 15, 'prihvacen'),
(20, 7, 15, 'odbijen'),
(21, 6, 14, 'prihvacen'),
(22, 7, 14, 'odbijen');

-- --------------------------------------------------------

--
-- Table structure for table `termin`
--

CREATE TABLE `termin` (
  `idT` int(11) NOT NULL,
  `idK` int(11) NOT NULL,
  `datumOd` datetime DEFAULT NULL,
  `datumDo` datetime DEFAULT NULL,
  `cena` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `termin`
--

INSERT INTO `termin` (`idT`, `idK`, `datumOd`, `datumDo`, `cena`) VALUES
(1, 6, '2016-04-05 20:00:00', '2016-04-05 21:00:00', 5000),
(2, 7, '2016-04-29 12:00:00', '2016-04-29 13:00:00', 3500),
(3, 16, '2016-05-04 01:00:00', '2017-06-05 01:00:00', 10000);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dogadjaj`
--
ALTER TABLE `dogadjaj`
  ADD PRIMARY KEY (`idD`),
  ADD KEY `fk_Dogadjaj_Osoba1_idx` (`idKO`),
  ADD KEY `fk_Dogadjaj_Hala1_idx` (`idKH`),
  ADD KEY `fk_Dogadjaj_Termin1_idx` (`idT`);

--
-- Indexes for table `galerija`
--
ALTER TABLE `galerija`
  ADD PRIMARY KEY (`idG`),
  ADD KEY `fk_Galerija_Hala1_idx` (`idK`);

--
-- Indexes for table `hala`
--
ALTER TABLE `hala`
  ADD PRIMARY KEY (`idK`),
  ADD KEY `fk_Hala_Korisnik_idx` (`idK`);

--
-- Indexes for table `komentar`
--
ALTER TABLE `komentar`
  ADD PRIMARY KEY (`idKom`),
  ADD KEY `fk_Komentar_Osoba1_idx` (`idK`),
  ADD KEY `fk_Komentar_Dogadjaj1_idx` (`idD`);

--
-- Indexes for table `korisnik`
--
ALTER TABLE `korisnik`
  ADD PRIMARY KEY (`idK`);

--
-- Indexes for table `ocena`
--
ALTER TABLE `ocena`
  ADD PRIMARY KEY (`idO`),
  ADD KEY `fk_osoba_has_osoba_osoba1_idx` (`idK`);

--
-- Indexes for table `osoba`
--
ALTER TABLE `osoba`
  ADD PRIMARY KEY (`idK`),
  ADD KEY `fk_table1_Korisnik1_idx` (`idK`);

--
-- Indexes for table `prijavljen`
--
ALTER TABLE `prijavljen`
  ADD PRIMARY KEY (`idP`),
  ADD KEY `fk_Prijavljen_Dogadjaj1_idx` (`idD`),
  ADD KEY `fk_Prijavljen_Osoba1_idx` (`idK`);

--
-- Indexes for table `termin`
--
ALTER TABLE `termin`
  ADD PRIMARY KEY (`idT`),
  ADD KEY `fk_Termin_Hala1_idx` (`idK`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `komentar`
--
ALTER TABLE `komentar`
  MODIFY `idKom` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `korisnik`
--
ALTER TABLE `korisnik`
  MODIFY `idK` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `ocena`
--
ALTER TABLE `ocena`
  MODIFY `idO` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `prijavljen`
--
ALTER TABLE `prijavljen`
  MODIFY `idP` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `termin`
--
ALTER TABLE `termin`
  MODIFY `idT` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `dogadjaj`
--
ALTER TABLE `dogadjaj`
  ADD CONSTRAINT `fk_Dogadjaj_Hala1` FOREIGN KEY (`idKH`) REFERENCES `hala` (`idK`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Dogadjaj_Osoba1` FOREIGN KEY (`idKO`) REFERENCES `osoba` (`idK`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Dogadjaj_Termin1` FOREIGN KEY (`idT`) REFERENCES `termin` (`idT`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `galerija`
--
ALTER TABLE `galerija`
  ADD CONSTRAINT `fk_Galerija_Hala1` FOREIGN KEY (`idK`) REFERENCES `hala` (`idK`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `hala`
--
ALTER TABLE `hala`
  ADD CONSTRAINT `fk_Hala_Korisnik` FOREIGN KEY (`idK`) REFERENCES `korisnik` (`idK`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `komentar`
--
ALTER TABLE `komentar`
  ADD CONSTRAINT `fk_Komentar_Dogadjaj1` FOREIGN KEY (`idD`) REFERENCES `dogadjaj` (`idD`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Komentar_Osoba1` FOREIGN KEY (`idK`) REFERENCES `osoba` (`idK`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `ocena`
--
ALTER TABLE `ocena`
  ADD CONSTRAINT `fk_osoba_has_osoba_osoba1` FOREIGN KEY (`idK`) REFERENCES `osoba` (`idK`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `osoba`
--
ALTER TABLE `osoba`
  ADD CONSTRAINT `fk_table1_Korisnik1` FOREIGN KEY (`idK`) REFERENCES `korisnik` (`idK`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `prijavljen`
--
ALTER TABLE `prijavljen`
  ADD CONSTRAINT `fk_Prijavljen_Dogadjaj1` FOREIGN KEY (`idD`) REFERENCES `dogadjaj` (`idD`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Prijavljen_Osoba1` FOREIGN KEY (`idK`) REFERENCES `osoba` (`idK`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `termin`
--
ALTER TABLE `termin`
  ADD CONSTRAINT `fk_Termin_Hala1` FOREIGN KEY (`idK`) REFERENCES `hala` (`idK`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
